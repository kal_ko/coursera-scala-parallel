import scalashop._
import scalashop.ScalaShop._


val w = 4
val h = 3
val src = new Img(w, h)
val dst = new Img(w, h)
src(0, 0) = 0; src(1, 0) = 1; src(2, 0) = 2; src(3, 0) = 9
src(0, 1) = 3; src(1, 1) = 4; src(2, 1) = 5; src(3, 1) = 10
src(0, 2) = 6; src(1, 2) = 7; src(2, 2) = 8; src(3, 2) = 11

boxBlurKernel(src, 0, 0, 2)

val numTasks = 3
val splits = 0 to src.height by ((src.height / numTasks) max 1)

splits.zip(splits.tail)


